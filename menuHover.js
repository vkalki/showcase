window.onload = function() {

    document.querySelector('#block_c').style.right = "0";
    document.querySelector('#block_b').style.right = "0";
    document.querySelector('#block_a').style.right = "0";

    // Rotate Header
    document.querySelector('#title_a').style.transform = 'rotate(-90deg)';

    // Expand Container & Content
    document.querySelector('#container_a').style.height = "75vh";
    document.querySelector('#content_a').style.height = "100vh";
}

var state_a = true; //left
var state_b = true; //mid
var state_c = true; //right

/* When hovering over the Welcome Block */
function toggleHoverLeft() {

    if (state_a) {
        hoverHelper("b", "c", "a");
        state_a = false;
    } else {
        state_a = true;
    }
}

/* When hovering over the Projects Block */
function toggleHoverMid() {

    if (state_b) {
        hoverHelper("a", "c", "b");
        state_b = false;
    } else {
        state_b = true;
    }
}

/* When hovering over the  Block */
function toggleHoverRight() {

    if (state_c) {
        hoverHelper("a", "b", "c");
        state_c = false;
    } else {
        state_c = true;
    }
}

function hoverHelper(shrink_a, shrink_b, grow) {

    // Shrink A
    document.querySelector('#title_' + shrink_a).style.transform = 'rotate(0deg)';
    document.querySelector('#container_' + shrink_a).style.height = "0vh";

    // Shrink B
    document.querySelector('#title_' + shrink_b).style.transform = 'rotate(0deg)';
    document.querySelector('#container_' + shrink_b).style.height = "0vh";

    // Expand Page
    document.getElementById("block_" + shrink_a).style.width = "15vw";
    document.getElementById("block_" + shrink_b).style.width = "15vw";
    document.getElementById("block_" + grow).style.width = "70vw";

    // Rotate Header
    document.querySelector('#title_' + grow).style.transform = 'rotate(-90deg)';

    // Expand Content
    document.querySelector('#container_' + grow).style.height = "75vh";
    document.querySelector('#content_' + grow).style.height = "auto";

}