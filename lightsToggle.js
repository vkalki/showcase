var state_color = true;

function toggleLights() {

    var lightPalette = "#F6F9FC";
    var darkPalette = "#222222";

    if (state_color) {
        document.body.style.background = lightPalette;
        document.body.style.color = darkPalette;

        document.querySelectorAll(".block").forEach(element => {
            element.style.backgroundColor = lightPalette
        });

        document.querySelectorAll(".content").forEach(element => {
            element.style.backgroundColor = lightPalette
        });

        document.querySelectorAll(".subtitleBackground").forEach(element => {
            element.style.backgroundColor = "white"
        });

        document.querySelectorAll(".blockTitle").forEach(element => {
            element.style.backgroundColor = "white"
        });

        document.querySelectorAll(".blockText").forEach(element => {
            element.style.backgroundColor = "black"
            element.style.color = "white"
        });

        document.querySelectorAll(".fa").forEach(element => {
            element.style.color = darkPalette
        });

        document.getElementById("footer").style.backgroundColor = "#DCDCDC";

        state_color = false;

    } else {
        document.body.style.background = darkPalette
        document.body.style.color = lightPalette

        document.querySelectorAll(".block").forEach(element => {
            element.style.backgroundColor = darkPalette
        });

        document.querySelectorAll(".content").forEach(element => {
            element.style.backgroundColor = darkPalette
        });

        document.querySelectorAll(".subtitleBackground").forEach(element => {
            element.style.backgroundColor = "black"
        });

        document.querySelectorAll(".blockTitle").forEach(element => {
            element.style.backgroundColor = "black"
        });

        document.querySelectorAll(".blockText").forEach(element => {
            element.style.backgroundColor = "white"
            element.style.color = "black"
        });

        document.querySelectorAll(".fa").forEach(element => {
            element.style.color = lightPalette
        });

        document.getElementById("footer").style.backgroundColor = "#111";

        state_color = true;
    }

}